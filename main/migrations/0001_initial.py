# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.CharField(max_length=255)),
                ('timeshift', models.TimeField(null=True, blank=True)),
                ('status', models.CharField(default=b'0', max_length=1)),
                ('h1', models.CharField(default=None, max_length=255, blank=True)),
                ('codepage', models.CharField(default=None, max_length=10, blank=True)),
                ('title', models.CharField(default=None, max_length=255, blank=True)),
                ('err_msg', models.CharField(default=None, max_length=255, blank=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
