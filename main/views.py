from django.shortcuts import render
from main.models import Task


def index(request):
    tasks = Task.objects.exclude(status=Task.STATUS_NONE).all()

    return render(request, 'index.html', {'tasks': tasks})