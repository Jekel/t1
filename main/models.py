from django.db import models


class Task(models.Model):
    STATUS_OK = '1'
    STATUS_ERR = '2'
    STATUS_NONE = '0'

    url = models.CharField(max_length=255)
    timeshift = models.TimeField(null=True, blank=True)
    status = models.CharField(default=STATUS_NONE, max_length=1)
    h1 = models.CharField(max_length=255, default=None, blank=True)
    codepage = models.CharField(max_length=10, default=None, blank=True)
    title = models.CharField(max_length=255, default=None, blank=True)
    err_msg = models.CharField(max_length=255, default=None, blank=True)

    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '-'.join([self.url, self.title or 'None', self.h1 or 'None', self.codepage or 'None']).encode('utf-8')

    def to_template(self):
        s = ''
        s += self.url
        if not self.title and not self.h1:
            return s

        if self.title:
            s += " Title: %s " % self.title
        if self.h1:
            s += " H1: %s" % self.h1
        s += " Codepage: %s" % self.codepage
        return s.encode('utf-8')