# coding: utf8
from gevent import monkey
monkey.patch_all(socket=True, dns=True, time=True, select=True,thread=False, os=True, ssl=True, httplib=False, aggressive=True)

from django.core.management.base import BaseCommand
from main.models import Task
import requests
from gevent.pool import Pool
from gevent import sleep
from lxml.html import document_fromstring
from lxml.cssselect import CSSSelector

POOL_SIZE = 5

h1 = CSSSelector('h1')
title = CSSSelector('title')


def worker(task):
    url = task.url
    if task.timeshift:
        # если определен сдвиг, воркер ждет
        seconds_to_sleep = task.timeshift.second + task.timeshift.minute * 60 + task.timeshift.hour * 60 * 60
        sleep(seconds_to_sleep)

    try:
        response = requests.get(url)
    except Exception as e:
        task.err_msg = str(e)
        task.status = task.STATUS_ERR
        task.save()
        return

    if response.status_code != 200:
        task.err_msg = 'response code is not 200'
        task.status = task.STATUS_ERR
        task.save()
        return

    if 'html' not in response.headers['content-type']:
        task.status = task.STATUS_ERR
        task.err_msg = 'returned content does not contains html data'
        task.save()
        return

    doc = document_fromstring(response.text)
    task.codepage = response.encoding

    try:
        task.h1 = h1(doc)[0].text_content().strip()
    except IndexError:
        pass

    try:
        task.title = title(doc)[0].text_content().strip()
    except IndexError:
        pass

    task.status = task.STATUS_OK
    task.save()


class Command(BaseCommand):
    help = 'Fetch urls'

    def handle(self, *args, **options):
        tasks_qs = Task.objects.filter(status=Task.STATUS_NONE).order_by('-timeshift')
        # отправляем в пул задачи, первыми отрабатывают те что без временного сдвига, остальные ждут очереди
        p = Pool(POOL_SIZE)
        p.map(worker, tasks_qs)
        p.join()
